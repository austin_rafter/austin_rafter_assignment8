import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
/*
 *
 * Austin Rafter
 * CS 049J
 *
 *sets button names and number of times clicked in constructor
 * action performed prints the name and incremented number of times clicked for the object
 */

public class ClickListener implements ActionListener {
    int nButtonClicked;
    String strButtonToClick;
    public ClickListener(String strButtonName, int nButtonClickedTimes){
        strButtonToClick = strButtonName;
        nButtonClicked = nButtonClickedTimes;
    }

    public void actionPerformed(ActionEvent event) {
         System.out.println(strButtonToClick + " was clicked " + nButtonClicked++ + " times." );
    }
}
