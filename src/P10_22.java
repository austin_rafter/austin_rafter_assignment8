import javax.swing.*;
/*
 *
 * Austin Rafter
 * CS 049J
 *
 *creates new CarFrame object
 *
 * sets frame title and exit option
 * shows the carframe and animation
 */

public class P10_22 {
    public static void main(String[] args) {

        //create new car frame to show animated car
        JFrame frame = new CarFrame();
         frame.setTitle("An animated Car");
         frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
         frame.setVisible(true);
    }
}
