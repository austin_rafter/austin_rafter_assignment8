import javax.swing.*;
import java.awt.*;
/*
 *
 * Austin Rafter
 * CS 049J
 *create car from car class at starting position
 * moveCarBy will repaint the car at the specified translation
 */

public class CarComponent extends JComponent {

    private static final int CAR_X = 0;
    private static final int CAR_Y = 0;

    //create car at top left corner of frame
    private final Car carToMove;
    public CarComponent(){
        carToMove = new Car(CAR_X, CAR_Y);
    }

    //paint the car
     public void paintComponent(Graphics g) {

         Graphics2D g2 = (Graphics2D) g;
         carToMove.draw(g2);
         }

    /*
    translate car and repaint to animate
     */
    public void moveCarBy(int nXMovement, int nYMovement){
        if(getWidth() > getHeight()){
            nXMovement = nXMovement + (getWidth()/getHeight());
        }
        carToMove.translate(nXMovement , nYMovement);
        repaint();
    }
}
