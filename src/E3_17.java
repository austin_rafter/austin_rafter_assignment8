import javax.swing.*;
/*
 *
 * Austin Rafter
 * CS 049J
 *
 *creates frames for ellipses
 * creates ellipsesComponent object
 * adds ellipsescomponent to frame and shows the ellipses
 */
public class E3_17 {

    public static void main(String[] args) {
        //create frame
        JFrame frame = new JFrame();
        frame.setSize(150, 250);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        EllipsesComponent ellipse = new EllipsesComponent();
        //add ellipse to frame
        frame.add(ellipse);
        frame.setVisible(true);
    }
}
