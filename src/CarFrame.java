import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
/*
 *
 * Austin Rafter
 * CS 049J
 *
 * creates the frame to show the car
 *creates an action listener for the move car by with parameters
 *
 * creates a one second offset between repaints of car
 */

public class CarFrame extends JFrame {

    private static final int FRAME_WIDTH = 300;
    private static final int FRAME_HEIGHT = 400;

    private final CarComponent movingCar;


    //TimerListener will move the car by  getWidth/100 and getHeight/100 for x and y respectively
    class TimerListener implements ActionListener {
         public void actionPerformed(ActionEvent event) { movingCar.moveCarBy(getWidth()/100, getHeight()/100); }
    }

    public CarFrame() {

        //create new car component
         movingCar = new CarComponent();
         //add the car component to the frame
         add(movingCar);

         //set initial frame width and height
         setSize(FRAME_WIDTH, FRAME_HEIGHT);

         //create new timer listener object to move object
         ActionListener listener = new TimerListener();

         //set delay timer between repaints of the car
         final int DELAY = 1000; // Milliseconds between timer ticks
         Timer t = new Timer(DELAY, listener);
         t.start();
    }

}
