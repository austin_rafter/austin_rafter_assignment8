import javax.swing.*;
import java.awt.event.ActionListener;
/*
 *
 * Austin Rafter
 * CS 049J
 *
 *creates the frame and buttons to add to the frame
 * sets the buttons to new ClickListener objects
 * shows the frame
 */

public class E10_18 {
    private static final int FRAME_WIDTH = 300;
    private static final int FRAME_HEIGHT = 150;

    public static void main(String[] args) {
        //create frame
        JFrame frame = new JFrame();
        //create panel to add buttons
        JPanel panel = new JPanel();
        //create button to add to panel
         JButton button = new JButton("I am Button One!");
       //create button to add to panel
        JButton buttonTwo = new JButton("I am Button Two!");
        //add button to panel
         panel.add(button);
         //set button location
        button.setLocation(20 , 20);
        //add button to panel
         panel.add(buttonTwo);
         //set button location
        buttonTwo.setLocation(40 , 40);
        //add panel of buttons to frame
        frame.add(panel);


        //create click listener object for button one
         ActionListener listenerOne = new ClickListener("Button One", 0);
         //create click listener object for button two
        ActionListener listenerTwo = new ClickListener("Button Two", 0);
         button.addActionListener(listenerOne);
         buttonTwo.addActionListener(listenerTwo);

         frame.setSize(FRAME_WIDTH, FRAME_HEIGHT);
         frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
         frame.setVisible(true);


    }
}
