import javax.swing.*;
import java.awt.*;
import java.awt.geom.Ellipse2D;
/*
*
* Austin Rafter
* CS 049J
*
* creates two ellipses
* the outer one is filled black to be the outline
* the inner one is filled a bluish color to fill
* the ellipse
*
 */

public class EllipsesComponent extends JComponent {

    public void paintComponent(Graphics g){
        Graphics2D g2 = (Graphics2D) g;

        //create two ovals for outline and filled  in color
        Ellipse2D.Double ellipseOne =  new Ellipse2D.Double(0,0,getWidth(),getHeight());
        Ellipse2D.Double ellipseTwo =  new Ellipse2D.Double(1,1,getWidth() - 2,getHeight() - 3);

        //create outline oval with black color
        g2.setColor(Color.BLACK);
        g2.fill(ellipseOne);
        g2.draw(ellipseOne);

        //create filled in oval for color
        Color myColor = new Color(64,128,255);
        g2.setColor(myColor);
        g2.fill(ellipseTwo);
        g2.draw(ellipseTwo);
    }
}
